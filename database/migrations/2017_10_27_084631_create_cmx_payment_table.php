<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCmxPaymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cmx_payment', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('transaction_id', 155)->nullable();
			$table->string('merchant_id', 145)->nullable();
			$table->string('table_no', 45)->nullable();
			$table->boolean('status_payment')->nullable();
			$table->string('last_update', 45)->nullable();
			$table->float('total_amount', 10, 0)->nullable();
			$table->string('order_no', 155)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cmx_payment');
	}

}
