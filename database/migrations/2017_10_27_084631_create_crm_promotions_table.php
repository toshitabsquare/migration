<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmPromotionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_promotions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name')->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('promo_code', 100)->nullable();
			$table->string('image', 250)->nullable();
			$table->integer('provider_id')->nullable();
			$table->string('external_promo_code', 45)->nullable();
			$table->integer('discount_type')->nullable();
			$table->float('discount_value', 10, 0)->nullable();
			$table->boolean('tax_inclusive')->nullable();
			$table->integer('tier_id')->nullable();
			$table->integer('program_id')->nullable();
			$table->string('order_type_id', 300);
			$table->text('group_items', 65535);
			$table->boolean('is_active');
			$table->date('valid_from');
			$table->date('valid_till');
			$table->string('days_applicable', 16);
			$table->time('time_from');
			$table->time('time_to');
			$table->boolean('allow_multiple_use')->default(0);
			$table->boolean('is_deleted')->default(0);
			$table->float('minimum_order_value', 10, 0);
			$table->boolean('is_hidden')->default(0);
			$table->boolean('all_category')->default(0);
			$table->string('app_type', 100)->nullable()->default('1')->comment('0:SkipQ, 1:eMenu, default:1');
			$table->bigInteger('modified')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_promotions');
	}

}
