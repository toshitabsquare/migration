<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeedbackTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('feedback', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('type')->default(0)->comment('0:dish,1:service,2:comment');
			$table->string('plu')->nullable();
			$table->integer('dish_id')->nullable();
			$table->string('item_name', 50)->nullable();
			$table->integer('service_id')->nullable();
			$table->string('service_name', 100)->nullable();
			$table->integer('rating')->nullable();
			$table->text('comment', 65535)->nullable();
			$table->integer('order_id');
			$table->integer('customer_id');
			$table->integer('created_by');
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('created_in_app')->comment('Refer app_types table');
			$table->boolean('is_send')->default(0)->comment('0: not send; 1: sent');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('feedback');
	}

}
