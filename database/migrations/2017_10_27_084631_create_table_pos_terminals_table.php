<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTablePosTerminalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('table_pos_terminals', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('table_id')->nullable();
			$table->string('pos_terminal', 100)->nullable();
			$table->integer('created_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('table_pos_terminals');
	}

}
