<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tags', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('group_id')->default(0);
			$table->string('name', 250)->comment('Name of tag');
			$table->string('image', 250)->nullable()->default('')->comment('Image of tag');
			$table->integer('sequence')->default(1)->comment('Sequence of display');
			$table->boolean('is_active')->default(1)->comment('1 If the row is deleted temporarily(hidden)');
			$table->boolean('is_deleted')->default(0)->comment('1 If the row is deleted permanently');
			$table->bigInteger('last_update')->nullable()->default(0)->comment('When it was last modified');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tags');
	}

}
