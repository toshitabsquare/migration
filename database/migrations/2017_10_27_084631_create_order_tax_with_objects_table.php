<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderTaxWithObjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_tax_with_objects', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('order_id');
			$table->integer('object_type')->comment('1: sku, 2: cus. opt., 3: order discount_charge, 4: order type charge, 5: delivery zone charge');
			$table->integer('object_id');
			$table->integer('tax_rule_id');
			$table->integer('tax_id');
			$table->integer('sequence');
			$table->integer('type_apply')->comment('1: Base Price, 2: Cumulative');
			$table->float('tax_value', 10, 0)->comment('%');
			$table->float('tax_amount', 10, 0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_tax_with_objects');
	}

}
