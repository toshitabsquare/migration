<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePromotionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promotions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 250);
			$table->text('description', 65535)->nullable();
			$table->string('code', 100);
			$table->string('image', 250)->nullable();
			$table->boolean('restaurant_applicable');
			$table->string('order_type_id', 300);
			$table->boolean('customer_group_id');
			$table->text('group_items', 65535);
			$table->integer('discount_type');
			$table->float('discount_value', 10, 0);
			$table->boolean('is_active');
			$table->date('valid_from');
			$table->date('valid_till');
			$table->string('days_applicable', 16);
			$table->time('time_from');
			$table->time('time_to');
			$table->boolean('allow_multiple_use')->default(0);
			$table->boolean('is_deleted')->default(0);
			$table->integer('created_by');
			$table->float('minimum_order_value', 10, 0);
			$table->boolean('all_category')->default(0);
			$table->bigInteger('modified')->nullable()->default(1);
			$table->string('app_type', 100)->nullable()->default('0')->comment('0:SkipQ, 1:eMenu, default:0');
			$table->boolean('is_hidden')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promotions');
	}

}
