<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDishCustomizationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dish_customizations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('sku_id')->nullable()->default(0)->comment('Foreign key of Dish');
			$table->integer('cust_id')->default(0)->comment('Foreign key of customization');
			$table->integer('sequence')->nullable()->default(1)->comment('Sequence of display');
			$table->boolean('is_active')->nullable()->default(1)->comment('1 If the row is deleted temporarily(hidden)');
			$table->boolean('is_deleted')->nullable()->default(0)->comment('1 If the row is deleted permanently');
			$table->bigInteger('last_update')->nullable()->default(0)->comment('When it was last modified');
			$table->integer('created_by')->default(0);
			$table->integer('dish_id')->nullable()->default(0)->comment('eMenu column');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dish_customizations');
	}

}
