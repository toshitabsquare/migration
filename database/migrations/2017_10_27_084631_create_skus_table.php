<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('skus', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('dish_id')->default(0);
			$table->string('name', 100)->default('');
			$table->float('price1', 15)->nullable();
			$table->float('price2', 5)->default(0.00);
			$table->float('price3', 5)->default(0.00);
			$table->float('price4', 5)->default(0.00);
			$table->float('price5', 5)->default(0.00);
			$table->float('price6', 5)->default(0.00);
			$table->float('price7', 5)->default(0.00);
			$table->float('price8', 5)->default(0.00);
			$table->float('price9', 5)->default(0.00);
			$table->float('price10', 5)->default(0.00);
			$table->integer('sequence')->default(1);
			$table->bigInteger('last_update')->default(0);
			$table->boolean('is_active')->default(1);
			$table->boolean('is_deleted')->default(0);
			$table->integer('created_by')->default(0);
			$table->string('plu', 30)->nullable()->default('');
			$table->string('pos_plu', 250)->nullable();
			$table->integer('tax_rule_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('skus');
	}

}
