<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRestaurantMenusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('restaurant_menus', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('menu_name', 250)->nullable();
			$table->boolean('is_deleted')->nullable()->default(0);
			$table->boolean('is_active')->nullable()->default(1);
			$table->integer('created_by');
			$table->bigInteger('last_update')->default(1);
			$table->timestamp('created')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('modified')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('restaurant_menus');
	}

}
