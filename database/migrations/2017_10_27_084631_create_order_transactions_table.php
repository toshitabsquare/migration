<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_transactions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('order_no', 50);
			$table->integer('merchant_id');
			$table->string('table_no', 20);
			$table->float('total_amount', 5);
			$table->integer('customer_id');
			$table->integer('crm_customer_id')->nullable();
			$table->string('mobile_number', 20);
			$table->boolean('status');
			$table->dateTime('created_date');
			$table->timestamp('last_modified_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('last_transaction_id', 200);
			$table->string('currency_symbol', 10)->default('$');
			$table->boolean('tax_inclusive');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_transactions');
	}

}
