<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('languages', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 250)->nullable()->comment('Name of language');
			$table->boolean('is_active')->nullable()->default(1)->comment('1 If the row is deleted temporarily(hidden)');
			$table->boolean('is_deleted')->nullable()->default(0)->comment('1 If the row is deleted permanently');
			$table->bigInteger('last_update')->nullable()->default(0)->comment('When it was last modified');
			$table->string('g_code', 20)->default('');
			$table->string('image', 100)->default('flag.png');
			$table->integer('created_by')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('languages');
	}

}
