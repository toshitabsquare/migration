<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDiningBillsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dining_bills', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('dining_id')->comment('foreign key or dinings');
			$table->text('bill_info', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dining_bills');
	}

}
