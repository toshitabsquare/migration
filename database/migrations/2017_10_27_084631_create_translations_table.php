<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('translations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('row_id')->default(0);
			$table->string('table_name', 100)->default('');
			$table->string('column_name', 100)->default('');
			$table->string('language_id', 20)->default('0');
			$table->text('translation', 65535);
			$table->boolean('is_active')->default(1);
			$table->boolean('is_deleted')->default(0);
			$table->bigInteger('last_update')->default(0);
			$table->integer('created_by')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('translations');
	}

}
