<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTimeBasedDisplayDishesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('time_based_display_dishes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('dish_id');
			$table->integer('time_based_id');
			$table->bigInteger('last_update')->default(1);
			$table->integer('created_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('time_based_display_dishes');
	}

}
