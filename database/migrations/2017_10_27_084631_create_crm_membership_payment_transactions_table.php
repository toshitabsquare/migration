<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmMembershipPaymentTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_membership_payment_transactions', function(Blueprint $table)
		{
			$table->integer('crm_membership_payment_details_id', true);
			$table->integer('crm_membership_payment_link_id');
			$table->integer('crm_membership_payment_link_attempt_id');
			$table->string('payment_type', 20);
			$table->string('transaction_id', 100);
			$table->string('transaction_status', 50);
			$table->dateTime('created_date');
			$table->string('reason_code', 20);
			$table->text('reason_desc', 65535);
			$table->string('transaction_debug_id', 100);
			$table->text('transaction_object', 65535);
			$table->string('payer_email', 100);
			$table->string('payer_first_name', 100);
			$table->string('payer_last_name', 100);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_membership_payment_transactions');
	}

}
