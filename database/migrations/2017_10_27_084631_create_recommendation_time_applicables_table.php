<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecommendationTimeApplicablesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recommendation_time_applicables', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('set_id');
			$table->time('to_time');
			$table->time('until_time');
			$table->bigInteger('last_update');
			$table->integer('created_by');
			$table->boolean('is_active')->default(1);
			$table->boolean('is_deleted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recommendation_time_applicables');
	}

}
