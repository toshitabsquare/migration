<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubSubCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sub_sub_categories', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('category_id')->nullable()->default(0);
			$table->integer('sub_category_id')->nullable()->default(0);
			$table->string('name', 250)->default('')->comment('Name of Sub Category');
			$table->text('header_text', 65535)->nullable();
			$table->string('image', 250)->default('default.png')->comment('Image file name');
			$table->integer('sequence')->default(1)->comment('Sequence for display');
			$table->integer('display_mode')->nullable()->default(0);
			$table->boolean('is_first_item')->default(0);
			$table->boolean('is_active')->default(1)->comment('1 If the row is deleted temporarily(hidden)');
			$table->boolean('is_deleted')->default(0)->comment('1 If the row is deleted permanently');
			$table->bigInteger('last_update')->default(0);
			$table->integer('created_by')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sub_sub_categories');
	}

}
