<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderDiscountChargesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_discount_charges', function(Blueprint $table)
		{
			$table->integer('id', true)->comment('id of order discount or charge');
			$table->string('name')->comment('name of discount or charge');
			$table->boolean('type')->comment('0:Discount,1:Charge');
			$table->boolean('type_of_discount_or_charge')->comment('0:Percentage,1:Dollar');
			$table->float('value', 10, 0)->comment('value of discount/charge');
			$table->integer('tax_rule_id')->nullable();
			$table->integer('created_by')->comment('id of restaurant');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_discount_charges');
	}

}
