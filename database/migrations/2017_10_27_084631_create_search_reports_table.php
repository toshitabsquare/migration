<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSearchReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('search_reports', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 128);
			$table->string('report_name', 128)->comment('bill, sale, feedback');
			$table->binary('criteria', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('search_reports');
	}

}
