<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmProgramCustomerMapTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_program_customer_map', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('program_id')->nullable();
			$table->integer('customer_id')->nullable();
			$table->integer('tier_id')->nullable();
			$table->boolean('is_active')->nullable()->default(1);
			$table->boolean('is_deleted')->nullable()->default(0);
			$table->bigInteger('last_update')->nullable();
			$table->boolean('is_receive_update')->nullable()->default(0);
			$table->boolean('is_receive_sms')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_program_customer_map');
	}

}
