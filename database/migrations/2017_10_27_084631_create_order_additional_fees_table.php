<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderAdditionalFeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_additional_fees', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('order_id')->comment('1: Taxation, 2: Promotion');
			$table->boolean('type');
			$table->integer('amount')->comment('Can be store negative value');
			$table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_additional_fees');
	}

}
