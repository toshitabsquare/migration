<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBillingOrderTaxesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('billing_order_taxes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name');
			$table->float('value', 10, 0);
			$table->integer('sequence');
			$table->integer('dining_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('billing_order_taxes');
	}

}
