<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRestaurantMenuCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('restaurant_menu_categories', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('restaurant_menu_id');
			$table->integer('category_id');
			$table->timestamp('created')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('modified')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('restaurant_menu_categories');
	}

}
