<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 100);
			$table->string('location', 100)->nullable()->default(' ');
			$table->text('description', 65535)->nullable();
			$table->string('username', 100)->unique('username');
			$table->string('password', 100);
			$table->string('lattitude', 100)->nullable();
			$table->string('longitude', 100)->nullable();
			$table->text('address', 65535)->nullable();
			$table->string('email', 100)->nullable();
			$table->text('website', 65535)->nullable();
			$table->string('phone', 15)->nullable();
			$table->integer('order_discount')->nullable();
			$table->boolean('inclusive_gst')->nullable();
			$table->string('support_mail', 50)->nullable();
			$table->string('currency_type', 100)->default('S$ ');
			$table->integer('type')->comment('1: supper admin
2: chain admin
restaurant admin');
			$table->boolean('is_live')->default(0);
			$table->text('customer_key', 65535)->nullable();
			$table->integer('created_by')->default(0);
			$table->integer('last_update')->default(1);
			$table->dateTime('last_login')->nullable();
			$table->boolean('is_tabsquare_restaurant')->default(0);
			$table->text('certificate', 65535)->nullable();
			$table->text('pushwoosh_appilcation_code', 65535)->nullable();
			$table->text('tnc', 65535)->nullable();
			$table->boolean('show_pictures')->default(1);
			$table->string('special_section_title', 1000)->nullable();
			$table->string('special_section_image', 250);
			$table->text('special_section_content', 65535)->nullable();
			$table->smallInteger('limit_message')->nullable()->default(0);
			$table->smallInteger('used_message')->nullable()->default(0);
			$table->date('last_send_push')->nullable();
			$table->text('privacy', 65535)->nullable();
			$table->string('app_ios_link', 1000)->nullable();
			$table->string('fb_post_image', 250)->nullable();
			$table->string('fb_post_link', 1000)->nullable();
			$table->text('fb_post_text', 65535)->nullable();
			$table->text('tnc_r', 16777215)->nullable();
			$table->text('fb_post_title', 65535)->nullable();
			$table->text('fb_page_body', 65535)->nullable();
			$table->text('fb_page_title', 65535)->nullable();
			$table->boolean('is_share_fb')->nullable();
			$table->boolean('is_user_skipq')->default(0);
			$table->boolean('is_user_emenu')->default(0);
			$table->boolean('is_start_favourite')->nullable()->default(0);
			$table->boolean('is_default_request')->nullable()->default(0);
			$table->text('default_request', 65535)->nullable();
			$table->integer('ui_type')->nullable()->default(2);
			$table->string('sku_header_text', 100)->nullable()->default('Please select an option');
			$table->integer('default_tax_rule_id')->nullable();
			$table->string('time_zone', 100)->nullable()->default('Asia/Singapore');
			$table->boolean('is_override_app_setting')->nullable()->default(0)->comment('this value return to emenu app');
			$table->boolean('is_hide')->nullable()->default(0)->comment('0: Show, 1: Hide');
			$table->text('more_info', 65535)->nullable()->comment('more info description of restaurant');
			$table->string('restaurant_image')->nullable()->default('restaurant_default_image.png')->comment('this column save image of restaurant');
			$table->boolean('feedback_emailer_type')->default(0)->comment('0: Send mail individually; 1: Send mail as a batch on a daily basis');
			$table->text('template_sms_payment', 65535)->nullable()->comment('This is template message of sms payment');
			$table->integer('masterId')->nullable();
			$table->float('rounded_type', 10, 0)->nullable();
			$table->float('rounded_value', 10, 0)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
