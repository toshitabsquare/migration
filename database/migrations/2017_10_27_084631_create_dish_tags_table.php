<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDishTagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dish_tags', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('tag_id')->default(0)->comment('Foreign key of Tag');
			$table->integer('dish_id')->default(0)->comment('Foreign Key of Dish');
			$table->boolean('is_active')->nullable()->default(1)->comment('1 If the row is deleted temporarily(hidden)');
			$table->boolean('is_deleted')->nullable()->default(0)->comment('1 If the row is deleted permanently');
			$table->bigInteger('last_update')->nullable()->default(0)->comment('When it was last modified');
			$table->integer('created_by')->default(0);
			$table->integer('sequence')->default(1);
			$table->boolean('is_display')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dish_tags');
	}

}
