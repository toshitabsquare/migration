<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderAnalyticsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_analytics', function(Blueprint $table)
		{
			$table->integer('order_id')->primary();
			$table->string('os_type', 20)->nullable();
			$table->string('os_version', 20)->nullable();
			$table->string('device_name', 50)->nullable();
			$table->string('app_version', 20)->nullable();
			$table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('created_day_of_week', 10)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_analytics');
	}

}
