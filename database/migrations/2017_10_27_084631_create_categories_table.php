<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categories', function(Blueprint $table)
		{
			$table->integer('id', true)->comment('PRIMARY KEY(Auto-Increment)');
			$table->string('name', 250)->comment('Name of category');
			$table->string('image', 250)->nullable()->default('default.png')->comment('Image file name');
			$table->string('thumbnail_image', 250)->nullable();
			$table->string('banner_image', 250)->nullable();
			$table->boolean('is_home_screen')->default(0);
			$table->integer('sequence')->nullable()->default(1)->comment('Sequence Display');
			$table->integer('display_mode')->nullable()->default(0);
			$table->boolean('is_active')->nullable()->default(1)->comment('1 If the row is deleted temporarily(hidden)');
			$table->boolean('is_deleted')->nullable()->default(0)->comment('1 If the row is deleted permanently');
			$table->bigInteger('last_update')->nullable()->default(0)->comment('When the row created/modified');
			$table->integer('created_by')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categories');
	}

}
