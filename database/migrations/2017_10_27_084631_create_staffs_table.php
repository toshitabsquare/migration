<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staffs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 100)->comment('Name of Staff');
			$table->string('email')->nullable()->default('');
			$table->string('username', 50)->nullable();
			$table->string('password', 50)->default('')->comment('Password of staff');
			$table->integer('created_by')->default(0)->comment('Restaurant id');
			$table->string('current_login_ip')->default('');
			$table->timestamp('last_login')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->text('user_token', 65535)->nullable();
			$table->text('app_token', 65535)->nullable();
			$table->text('device_token', 65535)->nullable();
			$table->integer('last_update')->nullable()->default(1);
			$table->boolean('online')->default(0);
			$table->text('socket_id', 65535)->nullable();
			$table->dateTime('last_connect')->nullable();
			$table->string('app_key')->nullable();
			$table->boolean('is_rider')->nullable()->default(0)->comment('1: is rider tikect 1909');
			$table->string('full_name', 250)->nullable()->comment('Full name of staff : design for ticket 1909');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('staffs');
	}

}
