<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTempPaymentOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('temp_payment_orders', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('order_transaction_id', 100);
			$table->bigInteger('time')->nullable()->default(1);
			$table->string('table_no')->nullable();
			$table->string('payment_transaction_id', 50)->nullable();
			$table->string('merchant_key', 50)->nullable();
			$table->string('payment_token')->nullable();
			$table->float('total_amount', 10, 0)->nullable();
			$table->string('send_from')->nullable();
			$table->string('payment_type')->nullable();
			$table->string('guid')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('temp_payment_orders');
	}

}
