<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmLoyaltyProvidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_loyalty_providers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('username', 45)->nullable();
			$table->string('password', 45)->nullable();
			$table->string('db_name', 45)->nullable();
			$table->string('enquiry_code', 45)->nullable();
			$table->string('outlet_code', 45)->nullable();
			$table->string('pos_id', 45)->nullable();
			$table->string('cashier_id', 45)->nullable();
			$table->string('URL')->nullable();
			$table->integer('restaurant_id')->nullable();
			$table->bigInteger('last_update')->nullable();
			$table->integer('provider_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_loyalty_providers');
	}

}
