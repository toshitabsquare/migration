<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('first_name', 250)->comment('Name of Customer');
			$table->string('last_name', 100)->nullable();
			$table->string('photo', 100)->nullable()->default('user_def.png');
			$table->string('phone', 250)->nullable()->comment('Phone of customer');
			$table->char('sex', 1)->default('M');
			$table->date('dob')->nullable();
			$table->string('password', 250)->nullable()->comment('Password for tabsquare account');
			$table->string('email')->nullable();
			$table->string('membership_id', 100)->nullable();
			$table->timestamp('last_login')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('tabsquare', 250)->nullable()->comment('Tabsquare login');
			$table->boolean('optional_promo')->nullable()->default(0);
			$table->timestamp('updated_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('created_by_staff_id')->nullable()->default(-1)->comment('Just for Phone order: This order is created by staff id');
			$table->string('pin', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers');
	}

}
