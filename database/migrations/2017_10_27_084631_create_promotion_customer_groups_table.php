<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePromotionCustomerGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promotion_customer_groups', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('promotion_id')->nullable();
			$table->integer('customer_group_id')->nullable();
			$table->boolean('is_delete')->nullable()->default(0);
			$table->integer('created_by')->nullable();
			$table->bigInteger('modified')->nullable()->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promotion_customer_groups');
	}

}
