<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderDeliveryInfosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_delivery_infos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('order_id');
			$table->boolean('status')->default(1)->comment('Refer delivery_statuses table');
			$table->integer('staff_id')->default(-1)->comment('-1 means staff is not assigned yet');
			$table->integer('rider_id')->nullable();
			$table->string('contact_no', 30);
			$table->date('delivery_date');
			$table->time('delivery_time');
			$table->string('address', 500);
			$table->integer('address_id');
			$table->integer('delivery_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_delivery_infos');
	}

}
