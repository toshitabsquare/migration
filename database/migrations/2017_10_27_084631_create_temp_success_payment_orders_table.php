<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTempSuccessPaymentOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('temp_success_payment_orders', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('order_transaction_id', 50)->nullable();
			$table->bigInteger('time')->nullable()->default(0);
			$table->string('merchant_key', 50)->nullable();
			$table->string('payment_transaction_id', 50)->nullable();
			$table->string('table_no', 100)->nullable();
			$table->string('payment_token')->nullable();
			$table->float('total_amount', 10, 0)->nullable();
			$table->string('send_from')->nullable();
			$table->string('payment_type')->nullable();
			$table->string('payment_mode')->nullable();
			$table->string('master_pass_token_id')->nullable();
			$table->string('guid')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('temp_success_payment_orders');
	}

}
