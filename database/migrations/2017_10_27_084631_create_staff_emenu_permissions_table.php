<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffEmenuPermissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staff_emenu_permissions', function(Blueprint $table)
		{
			$table->integer('staff_id')->primary();
			$table->boolean('void_item')->default(0)->comment('1 if he can make items void otherwise 0');
			$table->boolean('void_table')->default(0)->comment('1 if he can make table void otherwise 0');
			$table->boolean('reduce_item')->default(0)->comment('1 if he can reduce items otherwise 0');
			$table->boolean('checkout')->default(0)->comment('1 if he can checkout otherwise 0');
			$table->boolean('present_bill')->default(0)->comment('1 if he can present bill otherwise 0');
			$table->boolean('edit_order')->default(0)->comment('1 if he can edit order otherwise 0');
			$table->boolean('access_table_management')->default(0)->comment('1 if he can access table management otherwise 0');
			$table->boolean('modify_discounts')->default(0)->comment('1 if he can modify discount otherwise 0');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('staff_emenu_permissions');
	}

}
