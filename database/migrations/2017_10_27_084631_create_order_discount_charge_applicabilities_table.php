<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderDiscountChargeApplicabilitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_discount_charge_applicabilities', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('order_discount_charges_id');
			$table->boolean('order_type_id')->comment('Refer order_types table');
			$table->integer('created_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_discount_charge_applicabilities');
	}

}
