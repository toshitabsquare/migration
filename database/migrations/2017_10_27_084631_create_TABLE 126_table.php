<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTABLE126Table extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('TABLE 126', function(Blueprint $table)
		{
			$table->integer('COL 1')->nullable();
			$table->string('COL 2', 16)->nullable();
			$table->string('COL 3', 10)->nullable();
			$table->string('COL 4', 10)->nullable();
			$table->string('COL 5', 10)->nullable();
			$table->integer('COL 6')->nullable();
			$table->integer('COL 7')->nullable();
			$table->integer('COL 8')->nullable();
			$table->integer('COL 9')->nullable();
			$table->integer('COL 10')->nullable();
			$table->integer('COL 11')->nullable();
			$table->integer('COL 12')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('TABLE 126');
	}

}
