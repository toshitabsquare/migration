<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMasterdatarequestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('masterdatarequest', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('merchant_key', 20);
			$table->bigInteger('version_key');
			$table->text('data');
			$table->dateTime('created_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('masterdatarequest');
	}

}
