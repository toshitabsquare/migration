<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmenuPaymentInfosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('emenu_payment_infos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('allowed_payment_id');
			$table->text('key', 65535);
			$table->text('value', 65535);
			$table->integer('created_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('emenu_payment_infos');
	}

}
