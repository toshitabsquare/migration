<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmCmsActivationHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_cms_activation_histories', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('crm_customer_id');
			$table->integer('merchant_id');
			$table->integer('program_id');
			$table->decimal('total_amount', 10);
			$table->string('nric', 50);
			$table->string('type', 50);
			$table->dateTime('created_date');
			$table->string('currency_symbol');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_cms_activation_histories');
	}

}
