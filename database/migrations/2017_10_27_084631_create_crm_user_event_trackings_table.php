<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmUserEventTrackingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_user_event_trackings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('event_name');
			$table->string('external_customer_id');
			$table->integer('restaurant_id');
			$table->integer('program_id');
			$table->dateTime('created_date');
			$table->float('amount', 10);
			$table->string('payment_mode');
			$table->integer('tier_id');
			$table->string('currency_symbol', 10);
			$table->integer('status');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_user_event_trackings');
	}

}
