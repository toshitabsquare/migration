<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentLinkAttemptsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment_link_attempts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('payment_link_id');
			$table->dateTime('attempted_date_time')->nullable();
			$table->timestamp('last_modified_date_time')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('device_serial', 100);
			$table->string('os_type', 100);
			$table->string('browser_type', 100);
			$table->string('client_id', 100);
			$table->text('nonce', 65535);
			$table->boolean('status');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment_link_attempts');
	}

}
