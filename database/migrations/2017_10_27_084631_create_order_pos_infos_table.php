<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderPosInfosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_pos_infos', function(Blueprint $table)
		{
			$table->integer('order_id')->index('order_id');
			$table->string('transaction_order_id', 250)->comment('order_id of POS(Raptor ~ sale_no)');
			$table->string('table_no', 100)->default(' ')->index('table_no');
			$table->integer('pax')->nullable();
			$table->primary(['transaction_order_id','table_no','order_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_pos_infos');
	}

}
