<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmenuAllowedPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('emenu_allowed_payments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('payments_id');
			$table->text('base_url', 65535);
			$table->integer('created_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('emenu_allowed_payments');
	}

}
