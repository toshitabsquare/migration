<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePromotionUsagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promotion_usages', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('promotion_id');
			$table->integer('order_id');
			$table->integer('customer_id');
			$table->integer('customer_group_id');
			$table->integer('restaurant_id');
			$table->float('total_promo_discount', 10, 0);
			$table->float('total_bill_discount', 10, 0);
			$table->timestamp('updated_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('is_used')->nullable()->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promotion_usages');
	}

}
