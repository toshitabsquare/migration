<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDishCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dish_categories', function(Blueprint $table)
		{
			$table->integer('id', true)->comment('PRIMARY KEY(Auto-Increment)');
			$table->integer('dish_id')->default(0)->comment('Foreign key of Dish');
			$table->integer('category_id')->nullable()->default(0)->comment('Foreign key of Category');
			$table->integer('sub_category_id')->nullable()->default(0)->comment('Foreign key of  SubCategory');
			$table->integer('sub_sub_category_id')->nullable()->default(0)->comment('Foreign key of SubSubCategory');
			$table->integer('sequence')->nullable()->default(1)->comment('Sequence of display');
			$table->boolean('is_active')->nullable()->default(1)->comment('1 If the row is deleted temporarily(hidden)');
			$table->boolean('is_deleted')->nullable()->default(0)->comment('1 If the row is deleted permanently');
			$table->bigInteger('last_update')->nullable()->default(0)->comment('When it was last modified');
			$table->integer('created_by')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dish_categories');
	}

}
