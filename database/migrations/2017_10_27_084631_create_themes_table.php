<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateThemesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('themes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 250)->unique('name_2');
			$table->boolean('is_default')->nullable();
			$table->string('file_name', 100);
			$table->boolean('is_active');
			$table->boolean('is_deleted')->nullable()->default(0);
			$table->bigInteger('last_update');
			$table->integer('created_by');
			$table->boolean('type')->default(0)->comment('0: for eMenu IOS, 1: for eMenu Android');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('themes');
	}

}
