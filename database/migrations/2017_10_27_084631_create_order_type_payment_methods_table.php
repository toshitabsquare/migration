<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderTypePaymentMethodsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_type_payment_methods', function(Blueprint $table)
		{
			$table->integer('order_type_id');
			$table->integer('payment_method_id');
			$table->integer('res_id');
			$table->primary(['order_type_id','payment_method_id','res_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_type_payment_methods');
	}

}
