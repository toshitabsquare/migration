<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRestaurantTagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('restaurant_tags', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('tag_id');
			$table->string('image');
			$table->integer('created_by');
			$table->bigInteger('last_update');
			$table->boolean('is_displayed_in_emenu')->default(0);
			$table->boolean('is_displayed_in_search_tag')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('restaurant_tags');
	}

}
