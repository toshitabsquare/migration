<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTagGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tag_groups', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 100)->default('')->comment('Name of tag');
			$table->integer('sequence')->default(1)->comment('Sequence of display');
			$table->boolean('is_deleted')->default(0)->comment('1 If the row is deleted permanently');
			$table->bigInteger('last_update')->nullable()->default(0)->comment('When it was last modified');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tag_groups');
	}

}
