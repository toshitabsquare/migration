<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePromotionDishesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promotion_dishes', function(Blueprint $table)
		{
			$table->integer('promotion_id');
			$table->integer('dish_id');
			$table->string('short_title', 20);
			$table->primary(['promotion_id','dish_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promotion_dishes');
	}

}
