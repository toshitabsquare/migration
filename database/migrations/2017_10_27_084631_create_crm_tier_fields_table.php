<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmTierFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_tier_fields', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('tier_id')->nullable();
			$table->string('field_name', 45)->nullable();
			$table->string('field_type', 45)->nullable();
			$table->boolean('is_required')->nullable();
			$table->boolean('is_active')->nullable()->default(1);
			$table->boolean('is_deleted')->nullable()->default(0);
			$table->bigInteger('last_update')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_tier_fields');
	}

}
