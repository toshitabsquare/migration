<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmenuPaymentKeysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('emenu_payment_keys', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('payment_type', 256);
			$table->string('title', 256);
			$table->string('key', 256);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('emenu_payment_keys');
	}

}
