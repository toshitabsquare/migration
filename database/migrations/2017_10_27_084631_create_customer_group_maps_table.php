<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerGroupMapsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_group_maps', function(Blueprint $table)
		{
			$table->integer('customer_group_id');
			$table->integer('customer_id');
			$table->integer('created_by')->nullable();
			$table->primary(['customer_group_id','customer_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_group_maps');
	}

}
