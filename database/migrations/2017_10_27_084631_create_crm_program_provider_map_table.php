<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmProgramProviderMapTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_program_provider_map', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('program_id')->nullable();
			$table->integer('provider_id')->nullable();
			$table->boolean('is_active')->nullable()->default(1);
			$table->boolean('is_deleted')->nullable()->default(0);
			$table->bigInteger('last_update')->nullable();
			$table->dateTime('active_from')->nullable()->comment('will be null for phrase 1');
			$table->dateTime('active_to')->nullable()->comment('will be null for phrase 1');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_program_provider_map');
	}

}
