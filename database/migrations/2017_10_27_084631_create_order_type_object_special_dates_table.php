<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderTypeObjectSpecialDatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_type_object_special_dates', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('restaurant_id');
			$table->integer('order_type_id');
			$table->date('special_date');
			$table->time('start_time');
			$table->time('end_time');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_type_object_special_dates');
	}

}
