<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTsfeaturesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tsfeatures', function(Blueprint $table)
		{
			$table->string('created_by')->comment('merchant_key');
			$table->boolean('IDP')->default(0);
			$table->boolean('CRM')->default(0);
			$table->integer('id', true);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tsfeatures');
	}

}
