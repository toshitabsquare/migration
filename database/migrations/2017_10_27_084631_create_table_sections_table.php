<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableSectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('table_sections', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('section_name', 100)->nullable()->default('');
			$table->integer('sequence')->default(1);
			$table->bigInteger('last_update')->default(0);
			$table->boolean('is_active')->default(1);
			$table->boolean('is_deleted')->default(0);
			$table->integer('created_by')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('table_sections');
	}

}
