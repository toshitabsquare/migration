<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomizationOptionsPricesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customization_options_prices', function(Blueprint $table)
		{
			$table->integer('option_id');
			$table->decimal('combo_price', 10, 0);
			$table->decimal('regular_price', 10, 0);
			$table->integer('created_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customization_options_prices');
	}

}
