<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDateDisplayRangesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('date_display_ranges', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('time_based_id');
			$table->date('to_date')->nullable();
			$table->date('until_date')->nullable();
			$table->boolean('all_day')->nullable()->default(0);
			$table->bigInteger('last_update')->default(1);
			$table->integer('created_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('date_display_ranges');
	}

}
