<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDishAnalyticsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dish_analytics', function(Blueprint $table)
		{
			$table->integer('dish_id')->primary();
			$table->bigInteger('total_unique_fav_count');
			$table->bigInteger('total_eMenu_fav_count');
			$table->bigInteger('total_skipque_fav_count');
			$table->integer('total_order_count');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dish_analytics');
	}

}
