<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmExtraProviderLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_extra_provider_logs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('api')->nullable();
			$table->string('url')->nullable();
			$table->text('request_data', 65535)->nullable();
			$table->text('respone_data', 65535)->nullable();
			$table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('nric', 20);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_extra_provider_logs');
	}

}
