<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTimeDisplayRangesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('time_display_ranges', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('time_based_id');
			$table->time('to_time')->nullable();
			$table->time('until_time')->nullable();
			$table->bigInteger('last_update')->default(1);
			$table->integer('created_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('time_display_ranges');
	}

}
