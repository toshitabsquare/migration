<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDateRecommendationRangesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('date_recommendation_ranges', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('set_id');
			$table->date('to_date')->nullable();
			$table->date('until_date')->nullable();
			$table->bigInteger('last_update')->default(1);
			$table->integer('created_by');
			$table->boolean('is_active')->default(1);
			$table->boolean('is_deleted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('date_recommendation_ranges');
	}

}
