<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmenuPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('emenu_payments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 100)->nullable();
			$table->text('image_url', 65535)->nullable();
			$table->dateTime('created')->nullable()->default('2014-12-04 00:00:00');
			$table->bigInteger('modified')->nullable()->default(1);
			$table->text('payment_type', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('emenu_payments');
	}

}
