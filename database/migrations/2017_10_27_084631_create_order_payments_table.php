<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_payments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('order_id');
			$table->float('amount', 10, 0);
			$table->boolean('status')->comment('0: Accept Payment By PaymentGateway, 1: UnPaid (for Cash), 2: Failed, 3: Success');
			$table->boolean('payment_id')->comment('refer payments table');
			$table->string('payment_type', 100)->nullable();
			$table->string('transaction_id', 200);
			$table->integer('customer_id')->nullable();
			$table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('created_by_staff_id')->nullable()->default(-1)->comment('Just for Phone order: This order is created by staff id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_payments');
	}

}
