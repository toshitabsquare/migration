<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerPasswordTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_password', function(Blueprint $table)
		{
			$table->integer('id')->default(0);
			$table->string('first_name', 250)->comment('Name of Customer');
			$table->string('last_name', 100)->nullable();
			$table->string('password', 250)->nullable()->comment('Password for tabsquare account');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_password');
	}

}
