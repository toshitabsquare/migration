<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderLogActionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_log_actions', function(Blueprint $table)
		{
			$table->integer('order_id')->primary();
			$table->string('table_no');
			$table->dateTime('request_created_date')->nullable();
			$table->integer('seq_no')->nullable();
			$table->dateTime('request_last_modified_date')->nullable();
			$table->dateTime('request_deleted_date')->nullable();
			$table->dateTime('bill_modified_date')->nullable();
			$table->text('bill_content', 65535)->nullable();
			$table->dateTime('bill_deleted_date')->nullable();
			$table->dateTime('payment_modified_date')->nullable();
			$table->text('payment_content', 65535)->nullable();
			$table->dateTime('payment_deleted_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_log_actions');
	}

}
