<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderItemDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_item_details', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('order_id')->index('order_id');
			$table->integer('dish_id');
			$table->string('dish_name', 250);
			$table->integer('quantity');
			$table->float('base_price', 10, 0);
			$table->float('price', 10, 0)->comment('price = base_price * quantity');
			$table->integer('sku_id');
			$table->string('sku_name', 100);
			$table->string('item_type', 1)->default('I')->comment('I: Item, R: Recommendation');
			$table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('special_instruction', 500)->nullable();
			$table->string('sale_ref', 50)->nullable();
			$table->string('promo_sale_ref', 50)->nullable();
			$table->float('sku_price', 10, 0);
			$table->integer('recommendation_set_id')->nullable();
			$table->integer('selected_sub_category_id')->nullable();
			$table->boolean('is_send_as_item')->nullable()->default(0);
			$table->boolean('is_promotion_item')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_item_details');
	}

}
