<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRuleItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rule_items', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('rule_id');
			$table->integer('sku_id');
			$table->boolean('is_main_item')->default(0)->comment('1: Main item');
			$table->dateTime('created_date');
			$table->integer('created_by');
			$table->bigInteger('last_updated');
			$table->boolean('is_deleted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rule_items');
	}

}
