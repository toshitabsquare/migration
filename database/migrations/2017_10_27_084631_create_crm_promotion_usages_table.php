<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmPromotionUsagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_promotion_usages', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('crm_promotion_id');
			$table->integer('order_id');
			$table->integer('crm_customer_id')->nullable();
			$table->integer('tier_id')->nullable();
			$table->integer('restaurant_id');
			$table->float('total_promo_discount', 10, 0);
			$table->float('total_bill_discount', 10, 0);
			$table->timestamp('updated_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('is_used')->nullable()->default(1);
			$table->string('external_promo_instance_id', 100);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_promotion_usages');
	}

}
