<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmEmailTemplatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_email_templates', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('email_from', 45)->nullable();
			$table->string('subject', 45)->nullable();
			$table->text('template')->nullable();
			$table->bigInteger('last_update')->nullable();
			$table->boolean('is_active')->nullable()->default(1);
			$table->boolean('is_deleted')->nullable()->default(0);
			$table->string('template_type', 45)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_email_templates');
	}

}
