<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_customers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nric', 45)->nullable();
			$table->string('email', 45)->nullable();
			$table->string('password', 100)->nullable();
			$table->string('phone', 45)->nullable();
			$table->string('first_name', 45)->nullable();
			$table->string('last_name', 45)->nullable();
			$table->dateTime('dob')->nullable();
			$table->string('country_code', 5)->nullable();
			$table->dateTime('created_date')->nullable();
			$table->dateTime('updated_date')->nullable();
			$table->boolean('is_active')->nullable();
			$table->boolean('is_deleted')->nullable();
			$table->bigInteger('last_update')->nullable();
			$table->integer('provider_id')->nullable();
			$table->string('card_no', 45)->nullable();
			$table->dateTime('last_login')->nullable();
			$table->boolean('should_change_password')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_customers');
	}

}
