<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveryZoneWithCustomerAddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('delivery_zone_with_customer_addresses', function(Blueprint $table)
		{
			$table->integer('delivery_zone_id');
			$table->integer('customer_address_id');
			$table->integer('restaurant_id');
			$table->timestamp('created_date')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->primary(['delivery_zone_id','customer_address_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('delivery_zone_with_customer_addresses');
	}

}
