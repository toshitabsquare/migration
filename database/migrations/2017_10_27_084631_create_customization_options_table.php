<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomizationOptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customization_options', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cust_id')->default(0)->comment('Foreign key of customization');
			$table->string('name', 250)->comment('Option name');
			$table->integer('dish_id')->default(0);
			$table->integer('sku')->nullable()->default(0);
			$table->float('price', 10, 0)->default(0)->comment('Price of Selection');
			$table->text('image', 65535)->nullable();
			$table->boolean('has_counter')->nullable()->default(0);
			$table->boolean('is_preselected')->nullable()->default(0);
			$table->boolean('is_preselected_locked')->nullable()->default(0);
			$table->boolean('max_qty_flag')->nullable()->default(0);
			$table->boolean('send_as_item')->nullable()->default(0);
			$table->integer('sequence')->nullable()->default(0)->comment('Sequence for display');
			$table->boolean('is_active')->nullable()->default(1)->comment('1 If the row is deleted temporarily(hidden)');
			$table->boolean('is_deleted')->nullable()->default(0)->comment('1 If the row is deleted permanently');
			$table->bigInteger('last_update')->default(0)->comment('When the row created/modified');
			$table->integer('created_by')->default(0);
			$table->boolean('is_paid')->nullable()->comment('Paid toggle');
			$table->string('plu', 30)->default('0');
			$table->integer('tax_rule_id')->nullable();
			$table->boolean('is_show_sku')->nullable()->default(0);
			$table->boolean('is_special')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customization_options');
	}

}
