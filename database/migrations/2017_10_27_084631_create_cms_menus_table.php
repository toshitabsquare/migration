<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCmsMenusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cms_menus', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('display_text', 100);
			$table->string('href', 100);
			$table->integer('parent');
			$table->integer('sequence');
			$table->boolean('last_child')->default(0);
			$table->boolean('show')->default(1);
			$table->integer('type')->default(3);
			$table->boolean('is_emenu_menu')->default(1);
			$table->boolean('is_skipq_menu')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cms_menus');
	}

}
