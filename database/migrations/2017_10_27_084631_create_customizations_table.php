<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomizationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customizations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 100)->nullable()->default('');
			$table->text('header_text', 16777215);
			$table->boolean('is_recommendation')->nullable()->default(0);
			$table->integer('max_selection')->nullable()->default(1);
			$table->integer('min_selection')->default(0);
			$table->integer('paid_after')->default(-1);
			$table->float('paid_price', 10)->default(0.00);
			$table->integer('sequence')->nullable()->default(1);
			$table->boolean('is_active')->nullable()->default(1);
			$table->boolean('is_deleted')->nullable()->default(0);
			$table->bigInteger('last_update')->default(0);
			$table->integer('created_by')->nullable()->default(0);
			$table->integer('plu')->nullable();
			$table->boolean('type')->nullable()->default(0);
			$table->boolean('show_image_customization')->nullable()->default(0);
			$table->boolean('is_2_columns_view')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customizations');
	}

}
