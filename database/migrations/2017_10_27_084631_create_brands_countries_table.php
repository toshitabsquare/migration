<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBrandsCountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('brands_countries', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('business_unit_id')->nullable();
			$table->integer('brand_id')->nullable();
			$table->integer('country_id')->nullable();
			$table->timestamps();
			$table->boolean('is_deleted')->nullable()->default(0);
			$table->boolean('is_active')->nullable()->default(1);
			$table->bigInteger('last_update')->nullable();
			$table->integer('restaurant_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('brands_countries');
	}

}
