<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmUpgradeRulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_upgrade_rules', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 45)->nullable();
			$table->string('frequency', 45)->nullable();
			$table->float('cost', 10)->nullable();
			$table->integer('from_tier')->nullable();
			$table->integer('to_tier')->nullable();
			$table->boolean('is_active')->nullable()->default(1);
			$table->boolean('is_deleted')->nullable()->default(0);
			$table->bigInteger('last_update')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_upgrade_rules');
	}

}
