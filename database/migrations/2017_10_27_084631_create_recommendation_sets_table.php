<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecommendationSetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recommendation_sets', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('customer_group_id')->default(0);
			$table->boolean('all_date')->default(1);
			$table->boolean('all_time')->default(1);
			$table->string('day_applicable')->default('2,3,4,5,6,7,8')->comment('Default: All (2,3,4,5,6,7,8)');
			$table->integer('pax')->default(0);
			$table->boolean('trigger_at_checkout')->default(1);
			$table->boolean('trigger_at_pay')->default(0);
			$table->boolean('trigger_by_timer')->default(0);
			$table->integer('timer_value')->comment('for e.g. 25 mins');
			$table->integer('timer_repeat')->default(0);
			$table->boolean('for_your_page')->default(0);
			$table->string('name');
			$table->string('title');
			$table->boolean('criteria_not_show');
			$table->boolean('is_active')->default(1);
			$table->boolean('is_deleted')->default(0);
			$table->bigInteger('last_update')->default(1);
			$table->integer('created_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recommendation_sets');
	}

}
