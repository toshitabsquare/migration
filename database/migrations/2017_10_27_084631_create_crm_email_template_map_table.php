<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmEmailTemplateMapTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_email_template_map', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('program_id')->nullable();
			$table->integer('provider_id')->nullable();
			$table->integer('email_template_id')->nullable();
			$table->integer('restaurant_id')->nullable();
			$table->bigInteger('last_update')->nullable();
			$table->boolean('is_deleted')->nullable()->default(0);
			$table->boolean('is_active')->nullable()->default(1);
			$table->integer('tier_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_email_template_map');
	}

}
