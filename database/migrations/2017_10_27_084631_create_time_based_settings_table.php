<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTimeBasedSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('time_based_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('sunday_enable')->nullable()->default(0);
			$table->boolean('monday_enable')->nullable()->default(0);
			$table->boolean('tuesday_enable')->nullable()->default(0);
			$table->boolean('wednesday_enable')->nullable()->default(0);
			$table->boolean('thursday_enable')->nullable()->default(0);
			$table->boolean('friday_enable')->nullable()->default(0);
			$table->boolean('saturday_enable')->nullable()->default(0);
			$table->boolean('all_days_in_week')->nullable()->default(1);
			$table->boolean('all_time')->nullable()->default(1);
			$table->boolean('all_day')->nullable()->default(1);
			$table->boolean('show_hide')->default(0)->comment('1: Show. 0: Hide');
			$table->boolean('is_active')->default(1);
			$table->boolean('is_deleted')->default(0);
			$table->timestamp('modified')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('created')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('created_by');
			$table->bigInteger('last_update')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('time_based_settings');
	}

}
