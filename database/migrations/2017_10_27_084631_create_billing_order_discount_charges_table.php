<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBillingOrderDiscountChargesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('billing_order_discount_charges', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name');
			$table->float('total_discount_charge', 10, 0);
			$table->integer('type')->comment('0: Discount; 1: Charge');
			$table->integer('dining_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('billing_order_discount_charges');
	}

}
