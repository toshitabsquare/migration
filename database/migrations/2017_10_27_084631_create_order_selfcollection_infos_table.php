<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderSelfcollectionInfosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_selfcollection_infos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('order_id');
			$table->string('contact_no', 30);
			$table->date('collection_date');
			$table->time('collection_time');
			$table->integer('status')->default(1)->comment('Refer selfcollection_statuses table');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_selfcollection_infos');
	}

}
