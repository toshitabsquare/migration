<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSystemParametersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('system_parameters', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('value', 1000)->comment('parameter value');
			$table->integer('identifier')->nullable();
			$table->string('description', 2000)->comment('the description of parameter');
			$table->integer('created_by')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('system_parameters');
	}

}
