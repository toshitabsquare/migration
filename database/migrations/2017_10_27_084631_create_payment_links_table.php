<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentLinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment_links', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('order_transaction_id');
			$table->string('mobile_number', 20);
			$table->text('full_link', 65535);
			$table->text('shortened_link', 65535);
			$table->dateTime('generated_date');
			$table->dateTime('expired_date_time');
			$table->boolean('status');
			$table->text('inactive_reason', 65535);
			$table->timestamp('last_modified_date')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment_links');
	}

}
