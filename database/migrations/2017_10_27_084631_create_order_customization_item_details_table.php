<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderCustomizationItemDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_customization_item_details', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('order_id');
			$table->integer('item_id');
			$table->integer('customization_id');
			$table->string('customization_name', 250);
			$table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->float('paid_after_amount', 10, 0)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_customization_item_details');
	}

}
