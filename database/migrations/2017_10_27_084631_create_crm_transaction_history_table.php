<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmTransactionHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_transaction_history', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('crm_customer_id')->nullable();
			$table->string('action', 45)->nullable();
			$table->float('points', 10)->nullable();
			$table->float('amount', 10)->nullable();
			$table->string('type', 45)->nullable()->comment('type of transactions like (earn, spend, etc?)');
			$table->integer('promotion_id')->nullable();
			$table->integer('restaurant_id')->nullable();
			$table->string('promotion_instance_id', 100)->nullable()->comment('the instance id of a promotion, something like BDAY20-ID01');
			$table->dateTime('created_date')->nullable();
			$table->dateTime('updated_date')->nullable();
			$table->string('table_no')->nullable();
			$table->string('sale_no')->nullable();
			$table->string('ReceiptNo')->nullable();
			$table->string('OutletCode')->nullable();
			$table->dateTime('TransactDate')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_transaction_history');
	}

}
