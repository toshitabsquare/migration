<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDishesSpecificTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dishes_specific', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('dish_id');
			$table->integer('restaurant_id');
			$table->boolean('is_active');
			$table->boolean('is_deleted');
			$table->boolean('is_removed');
			$table->dateTime('created')->nullable();
			$table->bigInteger('last_update');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dishes_specific');
	}

}
