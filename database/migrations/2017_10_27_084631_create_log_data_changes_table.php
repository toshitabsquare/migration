<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogDataChangesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('log_data_changes', function(Blueprint $table)
		{
			$table->integer('id', true)->comment('this is version ');
			$table->integer('log_type')->comment('type of log [ 1 : log table ] [ 2 : log reservation ]');
			$table->string('table_name')->comment('the name of table');
			$table->dateTime('happen_at')->comment('time to log');
			$table->string('happen_type', 1)->comment('The type of change : I, U, D');
			$table->string('data_key_1', 500)->comment('log data, depend on table name');
			$table->string('data_key_2', 500);
			$table->string('data_key_3', 2)->nullable()->comment('table status');
			$table->integer('created_by');
			$table->bigInteger('modified')->nullable()->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('log_data_changes');
	}

}
