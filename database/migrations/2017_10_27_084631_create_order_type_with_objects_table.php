<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderTypeWithObjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_type_with_objects', function(Blueprint $table)
		{
			$table->integer('order_type_id');
			$table->integer('object_id');
			$table->boolean('object_type')->comment('1: Dish, 2: Category, 3: Sub Category, 4: Sub Sub Category, 5: Promotion, 6: Recomendation');
			$table->timestamp('created_date')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('created_by');
			$table->integer('res_id');
			$table->primary(['order_type_id','object_id','object_type','res_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_type_with_objects');
	}

}
