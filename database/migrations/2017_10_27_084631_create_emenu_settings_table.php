<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmenuSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('emenu_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('key');
			$table->string('value')->nullable();
			$table->timestamp('last_update')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('created_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('emenu_settings');
	}

}
