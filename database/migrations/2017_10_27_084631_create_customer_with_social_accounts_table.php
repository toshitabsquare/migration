<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerWithSocialAccountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_with_social_accounts', function(Blueprint $table)
		{
			$table->integer('customer_id');
			$table->string('social_account_id', 50);
			$table->integer('social_network')->default(1)->comment('Refer social_networks table');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_with_social_accounts');
	}

}
