<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmMembershipPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_membership_payments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nric', 50);
			$table->integer('merchant_id');
			$table->string('table_no', 20);
			$table->float('total_amount', 5);
			$table->integer('program_id');
			$table->string('mobile_number', 20);
			$table->boolean('status');
			$table->dateTime('created_date');
			$table->timestamp('last_modified_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('last_transaction_id', 200);
			$table->string('currency_symbol', 10)->default('$');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_membership_payments');
	}

}
