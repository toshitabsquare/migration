<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTimersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('timers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('table_no', 11);
			$table->string('start_time', 50);
			$table->float('timer', 10, 0)->comment('timer in minutes');
			$table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('is_deleted')->default(0);
			$table->float('timer_before_stop_order', 10, 0);
			$table->float('timer_stop_order', 10, 0);
			$table->float('timer_after_stop_order', 10, 0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('timers');
	}

}
