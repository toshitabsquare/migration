<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('log_requests', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('request_link', 65535);
			$table->text('param', 65535)->nullable();
			$table->string('type', 10);
			$table->text('respone', 65535)->nullable();
			$table->integer('created_by');
			$table->dateTime('created_date');
			$table->dateTime('modified')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('log_requests');
	}

}
