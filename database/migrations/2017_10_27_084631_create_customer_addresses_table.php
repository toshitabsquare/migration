<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerAddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_addresses', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('customer_id');
			$table->string('country', 250)->nullable();
			$table->string('state', 250)->nullable();
			$table->string('city', 250)->nullable();
			$table->string('address', 500);
			$table->string('block_code', 100)->nullable();
			$table->string('zip_code', 100)->nullable();
			$table->string('type', 250)->nullable();
			$table->string('lattitude', 100)->nullable();
			$table->string('longitude', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_addresses');
	}

}
