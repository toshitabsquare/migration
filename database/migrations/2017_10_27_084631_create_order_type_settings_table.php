<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderTypeSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_type_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('restaurant_id');
			$table->integer('order_type_id');
			$table->float('minimum_value', 10, 0)->nullable();
			$table->float('maximum_value', 10, 0)->nullable();
			$table->float('special_additional_charge', 10, 0)->nullable();
			$table->float('special_additional_charge_percent', 10, 0)->nullable();
			$table->integer('lead_time')->nullable()->comment('minute');
			$table->time('Mon_start_time')->nullable();
			$table->time('Mon_end_time')->nullable();
			$table->time('Tue_start_time')->nullable();
			$table->time('Tue_end_time')->nullable();
			$table->time('Wed_start_time')->nullable();
			$table->time('Wed_end_time')->nullable();
			$table->time('Thu_start_time')->nullable();
			$table->time('Thu_end_time')->nullable();
			$table->time('Fri_start_time')->nullable();
			$table->time('Fri_end_time')->nullable();
			$table->time('Sat_start_time')->nullable();
			$table->time('Sat_end_time')->nullable();
			$table->time('Sun_start_time')->nullable();
			$table->time('Sun_end_time')->nullable();
			$table->integer('tax_rule_id')->nullable();
			$table->boolean('is_active')->default(1);
			$table->float('special_discount', 10, 0)->nullable();
			$table->float('special_discount_percent', 10, 0)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_type_settings');
	}

}
