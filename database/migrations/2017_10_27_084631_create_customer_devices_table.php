<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerDevicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_devices', function(Blueprint $table)
		{
			$table->integer('customer_id')->default(0);
			$table->string('app_token', 100);
			$table->string('device_token', 300);
			$table->timestamp('last_login')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('is_active')->nullable()->default(1);
			$table->primary(['customer_id','app_token','device_token']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_devices');
	}

}
