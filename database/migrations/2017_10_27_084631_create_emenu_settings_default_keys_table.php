<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmenuSettingsDefaultKeysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('emenu_settings_default_keys', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('key');
			$table->string('name');
			$table->integer('group')->comment('setting group');
			$table->string('default_value')->comment('default value');
			$table->string('type')->comment('setting type');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('emenu_settings_default_keys');
	}

}
