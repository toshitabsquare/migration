<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rules', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('error_message_display')->nullable()->default('');
			$table->string('rule_name', 250);
			$table->boolean('is_active')->default(1);
			$table->boolean('is_deleted')->default(0);
			$table->boolean('is_combo_price')->default(0)->comment('0 Addon 1 combo price');
			$table->integer('max_allowed_selection')->nullable()->default(0);
			$table->integer('created_by');
			$table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->bigInteger('last_updated')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rules');
	}

}
