<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerRestaurantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_restaurants', function(Blueprint $table)
		{
			$table->integer('customer_id');
			$table->integer('restaurant_id');
			$table->boolean('created_in_app')->comment('Refer app_types table');
			$table->timestamp('created_date')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->primary(['customer_id','restaurant_id','created_in_app']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_restaurants');
	}

}
