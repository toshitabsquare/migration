<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDatePriceRangesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('date_price_ranges', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('time_based_price_setting_id');
			$table->date('to_date')->nullable();
			$table->date('until_date')->nullable();
			$table->bigInteger('last_update')->default(1);
			$table->integer('created_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('date_price_ranges');
	}

}
