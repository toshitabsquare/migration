<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmTiersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_tiers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('tier_code', 45);
			$table->string('name', 45)->nullable();
			$table->integer('program_id')->nullable();
			$table->boolean('level')->nullable();
			$table->bigInteger('last_update')->nullable();
			$table->boolean('is_active')->nullable()->default(1);
			$table->boolean('is_deleted')->nullable()->default(0);
			$table->dateTime('active_from')->nullable();
			$table->dateTime('active_to')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_tiers');
	}

}
