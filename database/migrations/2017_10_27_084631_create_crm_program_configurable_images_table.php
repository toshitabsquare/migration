<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmProgramConfigurableImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_program_configurable_images', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('program_id')->nullable();
			$table->integer('restaurant_id')->nullable();
			$table->integer('tier_id')->nullable();
			$table->string('component_key', 45)->nullable();
			$table->boolean('is_display')->nullable()->default(1);
			$table->boolean('is_active')->nullable()->default(1);
			$table->boolean('is_deleted')->nullable()->default(0);
			$table->bigInteger('last_update')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_program_configurable_images');
	}

}
