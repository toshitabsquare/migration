<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubCategoriesSpecificTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sub_categories_specific', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('sub_category_id');
			$table->integer('restaurant_id');
			$table->boolean('is_active');
			$table->bigInteger('last_update');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sub_categories_specific');
	}

}
