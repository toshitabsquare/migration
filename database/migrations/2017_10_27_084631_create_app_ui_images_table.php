<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAppUiImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('app_ui_images', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('image_location', 250)->nullable()->comment('App page name where image to be applied');
			$table->string('image_filename', 250)->nullable()->comment('File name');
			$table->string('image_title', 250);
			$table->boolean('is_active')->nullable()->default(1)->comment('1 If the row is deleted temporarily(hidden)');
			$table->boolean('is_deleted')->nullable()->default(0)->comment('1 If the row is deleted permanently');
			$table->bigInteger('last_update')->nullable()->default(0)->comment('When it was last modified');
			$table->integer('created_by')->default(0);
			$table->dateTime('created')->nullable()->default('2014-12-04 00:00:00');
			$table->dateTime('modified')->nullable()->default('2014-12-04 00:00:00');
			$table->boolean('is_emenu_image')->default(0);
			$table->boolean('is_skipq_image')->default(0);
			$table->boolean('is_emenu_android')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('app_ui_images');
	}

}
