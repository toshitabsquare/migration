<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBillingItemsTaxesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('billing_items_taxes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('tax_name');
			$table->float('tax_value', 10, 0);
			$table->integer('sequence');
			$table->integer('dining_by_dishes_id')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('billing_items_taxes');
	}

}
