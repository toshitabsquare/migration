<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDevicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('devices', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('customer_id');
			$table->string('merchant_id', 45);
			$table->string('operating_system', 45)->nullable();
			$table->string('firebase_token');
			$table->string('table_no', 45);
			$table->dateTime('last_update')->nullable();
			$table->string('device_id', 45);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('devices');
	}

}
