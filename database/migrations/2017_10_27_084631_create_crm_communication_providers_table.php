<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmCommunicationProvidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_communication_providers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('provider_id')->nullable();
			$table->integer('restaurant_id')->nullable();
			$table->string('username', 45)->nullable();
			$table->string('password', 45)->nullable();
			$table->string('customer_website_url', 45)->nullable();
			$table->string('restaurant_logo', 250)->nullable();
			$table->string('tabsquare_logo', 250)->nullable();
			$table->timestamps();
			$table->boolean('is_deleted')->nullable()->default(0);
			$table->boolean('is_active')->nullable()->default(1);
			$table->bigInteger('last_update')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_communication_providers');
	}

}
