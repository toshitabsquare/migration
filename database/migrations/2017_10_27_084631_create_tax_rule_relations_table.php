<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTaxRuleRelationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tax_rule_relations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('tax_rule_id');
			$table->integer('tax_id');
			$table->integer('type_apply')->comment('Base Price: 1, Cumulative: 2');
			$table->integer('sequence')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tax_rule_relations');
	}

}
