<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFavoritesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('favorites', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('dish_id');
			$table->integer('customer_id');
			$table->integer('created_by');
			$table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('created_in_app')->comment('Refer app_types table');
			$table->boolean('is_active')->default(1)->comment('1: favourite, 0: un-favourite');
			$table->integer('crm_customer_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('favorites');
	}

}
