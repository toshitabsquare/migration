<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDishesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dishes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('plu', 250)->nullable()->comment('UNIQUE identifier for billing purpose');
			$table->string('name', 250)->nullable()->comment('Name');
			$table->string('name2', 300)->default('');
			$table->string('image', 250)->nullable()->default('default.png')->comment('Image file name');
			$table->text('description', 16777215)->nullable()->comment('Description about dish');
			$table->boolean('is_combo')->nullable()->default(0)->comment('If Dish is type of como');
			$table->boolean('has_sku')->nullable()->default(0)->comment('If dish has different SKUs in case of beverage');
			$table->boolean('is_only_group_dish')->nullable()->default(0)->comment('If the dish is to be displayed only in case of combo items');
			$table->boolean('has_stock')->default(1);
			$table->float('price1', 10, 0)->nullable()->default(0)->comment('Price(Cumpolsory)');
			$table->float('price2', 10, 0)->nullable()->default(0)->comment('Price(Optional)');
			$table->float('price3', 10, 0)->nullable()->default(0)->comment('Price(Optional)');
			$table->float('price4', 10, 0)->nullable()->default(0)->comment('Price(Optional)');
			$table->float('price5', 10, 0)->nullable()->default(0)->comment('Price(Optional)');
			$table->float('price6', 10, 0)->nullable()->default(0)->comment('Price(Optional)');
			$table->float('price7', 10, 0)->nullable()->default(0)->comment('Price(Optional)');
			$table->float('price8', 10, 0)->nullable()->default(0)->comment('Price(Optional)');
			$table->float('price9', 10, 0)->nullable()->default(0)->comment('Price(Optional)');
			$table->float('price10', 10, 0)->nullable()->default(0)->comment('Price(Optional)');
			$table->integer('promotion_id')->nullable()->comment('Foreign Key of Promotion');
			$table->boolean('is_active')->nullable()->default(1)->comment('eMenu: determine STOCK OUT');
			$table->boolean('is_deleted')->nullable()->default(0)->comment('eMenu: determine HIDE/UNHIDE');
			$table->bigInteger('last_update')->nullable()->default(1);
			$table->integer('open_item_type_id')->default(0);
			$table->integer('created_by')->default(0);
			$table->timestamp('created')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('modified')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('is_removed')->default(0);
			$table->boolean('is_autotranslate_name')->nullable()->default(0);
			$table->boolean('is_autotranslate_desc')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dishes');
	}

}
