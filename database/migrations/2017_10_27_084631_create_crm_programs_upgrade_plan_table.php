<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrmProgramsUpgradePlanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_programs_upgrade_plan', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('program_id');
			$table->integer('upgrade_plan_id');
			$table->boolean('is_deleted')->nullable();
			$table->boolean('is_active')->nullable();
			$table->bigInteger('last_update')->nullable();
			$table->dateTime('start_date')->nullable();
			$table->dateTime('end_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_programs_upgrade_plan');
	}

}
