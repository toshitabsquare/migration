<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('order_no', 50);
			$table->float('order_amount', 5)->default(0.00);
			$table->float('total_amount', 5)->default(0.00);
			$table->integer('restaurant_id')->index('restaurant_id');
			$table->integer('customer_id');
			$table->integer('crm_customer_id')->nullable();
			$table->boolean('status')->default(1)->index('status')->comment('1: Confirmed');
			$table->integer('order_type_id')->index('order_type_id');
			$table->boolean('app_type')->default(2)->comment('Refer app_types table');
			$table->string('currency_symbol', 10)->default('$');
			$table->timestamp('changed_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('created_date for order');
			$table->boolean('restIncludeGst')->nullable()->default(0);
			$table->boolean('is_merchant')->nullable()->default(0);
			$table->integer('created_by_staff_id')->nullable()->default(-1)->comment('Just for Phone order: This order is created by staff id');
			$table->boolean('is_void')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
