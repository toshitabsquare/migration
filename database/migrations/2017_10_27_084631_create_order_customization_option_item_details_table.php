<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderCustomizationOptionItemDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_customization_option_item_details', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('order_id');
			$table->integer('cus_item_id');
			$table->integer('customization_option_id');
			$table->string('customization_option_name', 250);
			$table->float('base_price', 10, 0);
			$table->float('price', 10, 0);
			$table->integer('quantity');
			$table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->float('amount', 10, 0)->default(0);
			$table->boolean('is_send_as_item')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_customization_option_item_details');
	}

}
