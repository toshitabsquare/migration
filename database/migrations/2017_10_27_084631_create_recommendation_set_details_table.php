<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecommendationSetDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recommendation_set_details', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('set_id');
			$table->integer('category_id');
			$table->integer('item_id');
			$table->integer('sequence')->default(1);
			$table->bigInteger('last_update')->default(1);
			$table->integer('created_by');
			$table->boolean('is_active')->default(1);
			$table->boolean('is_deleted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recommendation_set_details');
	}

}
