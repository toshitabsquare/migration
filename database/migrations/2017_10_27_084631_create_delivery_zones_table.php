<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveryZonesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('delivery_zones', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('restaurant_id');
			$table->float('minimum_value', 10, 0)->comment('Minimum value allowed for Delivery');
			$table->float('maximum_value', 10, 0)->nullable()->comment('Maximum value allowed for Delivery (optional)');
			$table->float('minimum_value_for_free', 10, 0)->nullable()->comment('Minimum value for Delivery beyond which delivery is free (optional)');
			$table->text('delivery_zone', 65535);
			$table->float('special_additional_charge', 10, 0)->nullable()->comment('Special additional charge $');
			$table->float('special_additional_charge_percent', 10, 0)->nullable()->comment('Special additional charge %');
			$table->integer('tax_rule_id')->nullable();
			$table->boolean('is_active')->nullable()->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('delivery_zones');
	}

}
