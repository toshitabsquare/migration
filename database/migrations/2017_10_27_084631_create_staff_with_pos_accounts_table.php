<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffWithPosAccountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staff_with_pos_accounts', function(Blueprint $table)
		{
			$table->integer('staff_id')->primary();
			$table->string('pos_user_name', 20)->nullable();
			$table->string('pos_password', 20);
			$table->string('pos_type', 20);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('staff_with_pos_accounts');
	}

}
