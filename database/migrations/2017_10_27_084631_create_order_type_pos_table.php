<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderTypePosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_type_pos', function(Blueprint $table)
		{
			$table->integer('order_type_id');
			$table->string('pos_id', 50);
			$table->integer('created_by');
			$table->primary(['order_type_id','pos_id','created_by']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_type_pos');
	}

}
