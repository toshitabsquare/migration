<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCondimentInfosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('condiment_infos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('condiment_key');
			$table->string('condiment_value');
			$table->boolean('is_active')->default(1);
			$table->boolean('is_deleted')->default(0);
			$table->bigInteger('last_update');
			$table->integer('created_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('condiment_infos');
	}

}
