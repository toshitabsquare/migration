<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTABLE125Table extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('TABLE 125', function(Blueprint $table)
		{
			$table->integer('COL 1')->nullable();
			$table->integer('COL 2')->nullable();
			$table->integer('COL 3')->nullable();
			$table->string('COL 4', 16)->nullable();
			$table->integer('COL 5')->nullable();
			$table->integer('COL 6')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('TABLE 125');
	}

}
