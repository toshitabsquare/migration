<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableLayoutsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('table_layouts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('section')->default(1)->comment('Foreign key of table TABLE_SECTION');
			$table->string('table_no', 20)->default('0')->comment('Table Name');
			$table->string('status', 10)->default('A')->comment('Status if its hold or open');
			$table->boolean('is_split')->default(0);
			$table->integer('split_no')->default(0);
			$table->boolean('is_custom_table')->default(0);
			$table->string('table_parent', 20)->default('0');
			$table->dateTime('lock_time')->default('0000-00-00 00:00:00')->comment('The time when staff lock this table');
			$table->integer('lock_owner')->default(0)->comment('Id of staff who lock the table');
			$table->string('lock_device_id')->default('')->comment('id of device lock the table');
			$table->integer('sequence')->default(1);
			$table->string('third_party_id')->default('0');
			$table->integer('created_by');
			$table->bigInteger('last_update')->default(0);
			$table->boolean('is_deleted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('table_layouts');
	}

}
